from django.db import models
from rest_framework import serializers

class Todo(models.Model):

    collection = 'todos'
    serializer_path = 'example_app.models.TodoSerializer'
    readonly_sync = True

    text = models.CharField(blank=True, null=True, max_length=255)
    done = models.BooleanField(default=False)

    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = '__all__'

from django_nosql.signals import (
    sync_readonly_db,
    sync_remove_readonly_db
)
